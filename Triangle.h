#pragma once
#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	Triangle(const Point a, const Point b, const Point c, const std::string type, const std::string name);
	~Triangle();
	
	double getArea() const override;

	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

};