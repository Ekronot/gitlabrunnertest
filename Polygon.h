#pragma once

#include "Shape.h"
#include <vector>

class Polygon : public Shape
{
public:

	// Constructor
	Polygon(const std::string type, const std::string name);
	
	// Destructor
	~Polygon();

	// Getters
	std::vector<Point> getPoints() const;

	// Methods
	virtual void move(const Point& other);
	virtual double getPerimeter() const;

protected:
	std::vector<Point> _points;
};