#include "Rectangle.h"
#include <iostream>

#define TOP_LEFT_POINT 0
#define BOTTOM_RIGHT_POINT 2

myShapes::Rectangle::Rectangle(const Point a, const double length, const double width, const std::string type, const std::string name): Polygon(type, name)
{
	// Meaning the points are making a line.
	if (width == 0 || length == 0)
	{
		std::cerr << "Length or Width can't be 0." << std::endl ;
		_exit(1); // exiting the program
	}
	this->_length = length;
	this->_width = width;
	_points.push_back(a);
	_points.push_back(Point(a.getX() + abs(length), a.getY()));
	_points.push_back(Point(a.getX() + abs(length), a.getY() + abs(width)));
	_points.push_back(Point(a.getX(), a.getY() + abs(width)));
}

myShapes::Rectangle::~Rectangle()
{}

double myShapes::Rectangle::getArea() const
{
	return abs(this->_length * this->_width);
}

void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[TOP_LEFT_POINT], _points[BOTTOM_RIGHT_POINT]);
}

void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[TOP_LEFT_POINT], _points[BOTTOM_RIGHT_POINT]);
}


