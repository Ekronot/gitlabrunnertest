#include "BetterCanvas.h"
#include <iostream>

BetterCanvas::BetterCanvas() {

}

BetterCanvas::~BetterCanvas() {

}

void BetterCanvas::drawShape(const Shape& shape, const Color color) {
	const std::string shapeType = shape.getType();

	if (shapeType == "Arrow") {
		this->canvas.draw_arrow(((Arrow&)shape).getSource(), ((Arrow&)shape).getDestination(), color);
	}
	else if (shapeType == "Circle") {
		this->canvas.draw_circle(((Circle&)shape).getCenter(), ((Circle&)shape).getRadius(), color);
	}
	else if (shapeType == "Triangle") {
		this->canvas.draw_triangle(((Triangle&)shape).getPoints()[0], ((Triangle&)shape).getPoints()[1], ((Triangle&)shape).getPoints()[2], color);
	}
	else if (shapeType == "Rectangle") {
		this->canvas.draw_rectangle(((myShapes::Rectangle&)shape).getPoints()[0], ((myShapes::Rectangle&)shape).getPoints()[2], color);
	}
}

void BetterCanvas::clearShape(const Shape& shape) {
	drawShape(shape, Color::black);
}