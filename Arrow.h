#pragma once
#include "Shape.h"

class Arrow: public Shape
{
public:
	
	// Constructor
	Arrow(const Point a, const Point b, const std::string type, const std::string name);
	
	// Destructor
	~Arrow();

	// Getters
	Point getSource() const;
	Point getDestination() const;

	// Methods
	double getPerimeter() const override; 
	double getArea() const override;
	void move(const Point& other) override;

	void draw(const Canvas& canvas) override;
	void clearDraw(const Canvas& canvas) override;

private:

	Point _source;
	Point _destination;
};

