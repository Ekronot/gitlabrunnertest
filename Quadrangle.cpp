#include "Quadrangle.h"
#include <iostream>


Quadrangle::Quadrangle(const Point a, const Point b, const Point c, const Point d, const std::string type, const std::string name) : Polygon(type, name) {
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
	this->_points.push_back(d);
}

double Quadrangle::getArea() const 
{
	// Calculating Quadrangle area by dividing it to 2 triangles
	// more information: https://byjus.com/maths/area-of-quadrilateral/

	double area1 = 0;	// area of the first triangle
	double area2 = 0;	// area of the second triangle

	double a = 0;
	double b = 0;
	double c = 0;
	double d = 0;

	// getting all sides of quadrangle
	a = this->_points[0].distance(this->_points[1]);
	b = this->_points[1].distance(this->_points[2]);
	c = this->_points[2].distance(this->_points[3]);
	d = this->_points[3].distance(this->_points[0]);

	// calculating area of triangle ABC
	area1 = (a + b + c) / 2;
	area1 = sqrt(area1 * (area1 - a) * (area1 - b) * (area1 - c));

	// calculating area of triangle BCD
	area2 = (b + c + d) / 2;
	area2 = sqrt(area2 * (area2 - b) * (area2 - c) * (area2 - d));

	// quadrangle area is the sum of both area
	return area1 + area2;
}

void Quadrangle::draw(const Canvas& canvas)
{
	// Drawing two triangles, forming together the quadrangle
	canvas.draw_triangle(_points[0], _points[1], _points[2], Color::purple);
	canvas.draw_triangle(_points[1], _points[2], _points[3], Color::purple);

	// alternatively, add a method to Canvas and use draw_polygon from CIMG
}

void Quadrangle::clearDraw(const Canvas& canvas)
{
	// Clearing the two triangles forming the quadrangle
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
	canvas.clear_triangle(_points[1], _points[2], _points[3]);

	// alternatively, add a method to Canvas and use draw_polygon from CIMG
}
